from archive.scr03 import WindowCapture
import cv2
import time
import numpy as np
from PIL import ImageGrab
from actions import (click_button, randomizer_sleep, which_is_likely, which_is_likely_triple, check_if_appeared, battle_end, search_for_image_on_battlemap, map_right,
                     )
from aurora import go_to_map_after_battle, gimme_the_loot, press_d3, press_switch, do_battle
from element_click import (menu_battle_press, press_escape, battle_confirm_press, daily_battle_press,
                           usual_daily_mid_battle_press, usual_daily_big_battle_press, usual_daily_short_battle_press,
                           usual_daily_shelling_press, battle_mini_press, press_lclick, hq_button_press, dorm_press,
                           supplies_press, snacks_5000_press, click_button, recognize_button)


def auto_farm(*strings_for_search):
    finder_counter = 0
    start_time = time.time()

    logo = cv2.imread(r'elements/memu_play.png')
    logo = np.array(logo)

    loop_time = time.time()
    confidence = 0.73

    screenshot = ImageGrab.grab()
    screenshot = np.array(screenshot)
    screenshot = cv2.cvtColor(screenshot, cv2.COLOR_RGB2BGR)

    try:
        match_logo = cv2.matchTemplate(screenshot, logo, cv2.TM_CCOEFF_NORMED)
        min_val_logo, max_val_logo, min_loc_logo, max_loc_logo = cv2.minMaxLoc(match_logo)
        logo_w_change = max_loc_logo[1]
        logo_h_change = max_loc_logo[0]
    except:
        print("Can't find logo")

    while True:
        screenshot = ImageGrab.grab()
        screenshot = np.array(screenshot)
        screenshot = cv2.cvtColor(screenshot, cv2.COLOR_RGB2BGR)
        current_time = time.time()

        if current_time - start_time >= 360:
            print("Time has come! ! ! ")
            piece_image = cv2.imread(r'aurora/switch.png')
            piece_image = np.array(piece_image)
            piece_image = np.array(piece_image)
            match = cv2.matchTemplate(screenshot, piece_image, cv2.TM_CCOEFF_NORMED)
            min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(match)

            print('Best match top left position: %s' % str(max_loc))
            print('Best match confidence: %s' % max_val)

            if max_val >= confidence:
                click_button(recognize_button(r'aurora/switch.png', confidence=confidence-0.005))
                randomizer_sleep(2)
                start_time = time.time()

        for i in strings_for_search:
            if finder_counter >= 300:
                for k in strings_for_search:
                    search_for_image_on_battlemap(k, confidence=confidence)
                    if search_for_image_on_battlemap(k, confidence=confidence):
                        break

            print("now working with: " + i)
            piece_image = cv2.imread(i)
            piece_image = np.array(piece_image)
            piece_image = np.array(piece_image)

            try:
                match = cv2.matchTemplate(screenshot, piece_image, cv2.TM_CCOEFF_NORMED)
            except:
                pass

            min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(match)

            print('Best match top left position: %s' % str(max_loc))
            print('Best match confidence: %s' % max_val)

            if max_val >= confidence:
                click_button(recognize_button(i, confidence=confidence-0.005))
                randomizer_sleep(3)
                finder_counter = 0

            print('FPS {}'.format(1 / (time.time() - loop_time)))
            loop_time = time.time()

            finder_counter += 1
            print(str(finder_counter) + " is now counter")

            if cv2.waitKey(1) == ord('q'):
                break


auto_farm(r'aurora/siren.png', r'aurora/siren_big2.png', r'aurora/siren_big.png', r'aurora/boss.png', r'aurora/button_battle_mini.png',
          r'aurora/s_rank.png', r'aurora/gimme_the_loot.png', r'aurora/battle_confirm.png', r'aurora/normal_enemy.png',
          r'aurora/easy_enemy.png', r'aurora/easiest_enemy.png')
print("Done")