"""
Do dailies:
Press battle
Press daily
Press Aviation\Shelling\Torpedo
Optional: check Info box, press notified, press Got It TODO
Press mini Battle
Wait for 2 minutes TODO: recognize end battle
Press Continue
Press Continue
Press Confirm
Press Escape
Press Escape
"""
from element_click import (menu_battle_press, press_escape, battle_confirm_press, daily_battle_press,
                           usual_daily_mid_battle_press, usual_daily_big_battle_press, usual_daily_short_battle_press,
                           usual_daily_shelling_press, battle_mini_press, press_lclick, hq_button_press, dorm_press,
                           supplies_press, snacks_5000_press, click_button, recognize_button)

from actions import (randomizer_sleep, battle_end, main_menu, check_if_appeared, click_button, recognize_button,
                     which_is_likely, which_is_likely_triple)
import pyautogui as pg


def infobar_clicker():
    if check_if_appeared(r'elements/first_battle_infobar.png'):
        click_button(recognize_button(r'elements/confirm_infobar.png'))


def daily_usual_training():
    menu_battle_press()
    randomizer_sleep(3)
    daily_battle_press()
    randomizer_sleep(2)
    if which_is_likely_triple(r'elements/usual_daily_small.png', r'elements/usual_daily_mid.png', r'elements/usual_daily_big.png') != r'elements/usual_daily_big.png':
        click_button(recognize_button(which_is_likely_triple(r'elements/usual_daily_small.png', r'elements/usual_daily_mid.png', r'elements/usual_daily_big.png'), confidence=0.3))
        randomizer_sleep(2)
        click_button(recognize_button(which_is_likely_triple(r'elements/usual_daily_small.png', r'elements/usual_daily_mid.png', r'elements/usual_daily_big.png'), confidence=0.3))
    else:
        click_button(recognize_button(which_is_likely_triple(r'elements/usual_daily_small.png', r'elements/usual_daily_mid.png', r'elements/usual_daily_big.png'), confidence=0.3))
    randomizer_sleep(2)
    usual_daily_shelling_press()
    randomizer_sleep(1.5)
    battle_mini_press()
    press_lclick()
    randomizer_sleep(0.5)

    battle_end()

    pg.leftClick()
    randomizer_sleep(1)
    pg.leftClick()
    randomizer_sleep(1)
    battle_confirm_press()
    randomizer_sleep(1)

    main_menu()

    randomizer_sleep(1)


def dorm_refilling():
    hq_button_press()
    randomizer_sleep(1.5)
    dorm_press()
    randomizer_sleep(5)
    # TODO: Confirm_blue click | Done
    if check_if_appeared(r'elements\button_dorm_confirm.png'):
        click_button(recognize_button(r'elements\button_dorm_confirm.png', confidence=0.3))
    randomizer_sleep(3)
    if check_if_appeared(r'elements\give_food.png'):
        click_button(recognize_button(r'elements\give_food.png'))
    # pg.move(400, 200, duration=0.5)
    # press_lclick()
    randomizer_sleep(0.5)
    supplies_press()
    randomizer_sleep(1)
    snacks_5000_press()
    randomizer_sleep(0.5)
    # TODO: realize refuel process by checking attention image
    for i in range(8):
        press_lclick()
        randomizer_sleep(0.5)
    pg.move(250, 250, duration=0.5)
    press_lclick()

    main_menu()


# TODO: test it
def daily_fierce_assault():
    menu_battle_press()
    randomizer_sleep(3)
    daily_battle_press()
    randomizer_sleep(2)
    if which_is_likely_triple(r'elements/fierce_small.png', r'elements/fierce_mid.png', r'elements/fierce_big.png') != r'elements/fierce_big.png':
        click_button(recognize_button(which_is_likely_triple(r'elements/fierce_small.png', r'elements/fierce_mid.png', r'elements/fierce_big.png')))
        randomizer_sleep(2)
        click_button(recognize_button(which_is_likely_triple(r'elements/fierce_small.png', r'elements/fierce_mid.png', r'elements/fierce_big.png')))
    else:
        click_button(recognize_button(which_is_likely_triple(r'elements/fierce_small.png', r'elements/fierce_mid.png', r'elements/fierce_big.png')))
    randomizer_sleep(2)
    """ At this point battle is selected??"""

    battle_mini_press()
    press_lclick()
    randomizer_sleep(0.5)

    battle_end()

    """ I'm not sure about that"""
    click_button(recognize_button(r'elements/button_continue.png'))
    randomizer_sleep(1)
    press_lclick()
    randomizer_sleep(1)
    press_lclick()
    randomizer_sleep(1)

    main_menu()


def daily_escort_mission():
    menu_battle_press()
    randomizer_sleep(3)
    daily_battle_press()
    randomizer_sleep(2)
    if which_is_likely_triple(r'elements/escort_small.png', r'elements/escort_mid.png', r'elements/escort_big.png') != r'elements/escort_big.png':
        click_button(recognize_button(which_is_likely_triple(r'elements/escort_small.png', r'elements/escort_mid.png', r'elements/escort_big.png')))
        randomizer_sleep(2)
        click_button(recognize_button(which_is_likely_triple(r'elements/escort_small.png', r'elements/escort_mid.png', r'elements/escort_big.png')))
    else:
        click_button(recognize_button(which_is_likely_triple(r'elements/escort_small.png', r'elements/escort_mid.png', r'elements/escort_big.png')))
    randomizer_sleep(2)

    click_button(recognize_button(r'elements/escort_option_firepower.png'))
    randomizer_sleep(2)

    battle_mini_press()
    press_lclick()
    randomizer_sleep(0.5)

    battle_end()

    """ I'm not sure about that"""
    click_button(recognize_button(r'elements/button_continue.png', confidence=0.2))
    randomizer_sleep(1)
    press_lclick()
    randomizer_sleep(1)
    press_lclick()
    randomizer_sleep(1)

    main_menu()