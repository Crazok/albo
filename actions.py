import cv2
import numpy as np
from PIL import ImageGrab
import pyautogui as pg
import random
import pydirectinput as pg2
import inspect
import sys
import time
import threading as th
import keyboard


# TODO: do algorithm through copypaste data


def recognize_button(image_to_found, confidence=0.8, method=cv2.TM_CCOEFF_NORMED):
    piece_img = cv2.imread(image_to_found, cv2.IMREAD_UNCHANGED)
    whole_img = ImageGrab.grab()
    whole_hash = np.array(whole_img)
    piece_hash = np.array(piece_img)
    whole_hash = cv2.cvtColor(whole_hash, cv2.COLOR_RGB2BGR)
    # piece_hash = cv2.cvtColor(piece_hash, cv2.COLOR_RGB2BGR)

    match = cv2.matchTemplate(whole_hash, piece_hash, method)

    # cv2.imshow("s", match)
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()

    min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(match)
    print('Operation with ' + str(image_to_found))
    print('Best match top left position: %s' % str(max_loc))
    print('Best match confidence %s' % max_val)

    threshold = confidence
    if max_val >= threshold:
        print("Found piece.")

        piece_w = piece_img.shape[1]
        piece_h = piece_img.shape[0]

        top_left = max_loc
        bottom_right = (top_left[0] + piece_w, top_left[1] + piece_h)

        center = (int(top_left[0] + piece_w / 2), int(top_left[1] + piece_h / 2))

        # cv2.rectangle(whole_hash, top_left, bottom_right, color=(0, 255, 255), thickness=2, lineType=cv2.LINE_4)
        # cv2.rectangle(whole_hash, top_left, center, color=(0, 0, 255), thickness=2, lineType=cv2.LINE_4)
        # cv2.imshow("s", whole_hash)
        # cv2.waitKey(0)
        return center
    else:
        print(str(max_val) + " - is maximum confidence value. Threshold is - " + str(threshold))


def recognize_button_top_left(image_to_found, confidence=0.8, method=cv2.TM_CCOEFF_NORMED):
    piece_img = cv2.imread(image_to_found, cv2.IMREAD_UNCHANGED)
    whole_img = ImageGrab.grab()
    whole_hash = np.array(whole_img)
    piece_hash = np.array(piece_img)
    whole_hash = cv2.cvtColor(whole_hash, cv2.COLOR_RGB2BGR)
    # piece_hash = cv2.cvtColor(piece_hash, cv2.COLOR_RGB2BGR)

    match = cv2.matchTemplate(whole_hash, piece_hash, method)

    # cv2.imshow("s", match)
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()

    min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(match)
    print('Operation with ' + str(image_to_found))
    print('Best match top left position: %s' % str(max_loc))
    print('Best match confidence %s' % max_val)
    # pg.moveTo(max_loc, duration=1)

    threshold = confidence
    if max_val >= threshold:
        return max_loc
    else:
        print(str(max_val) + " - is maximum confidence value. Threshold is - " + str(threshold))


def click_button(coordinate_pair):
    try:
        low_x = coordinate_pair[0] - 7
        high_x = coordinate_pair[0] + 7
        low_y = coordinate_pair[1] - 7
        high_y = coordinate_pair[1] + 7
        random_x = random.randint(low_x, high_x)
        random_y = random.randint(low_y, high_y)
        randomized_pair = (random_x, random_y)
        print("coordinates to move is " + str(randomized_pair))
        random_duration = random.randrange(10, 35) / 100
        pg.moveTo(randomized_pair, duration=random_duration)
        pg.leftClick()
    except:
        print("Nothing to click")


def click_out_of_image(coordinate_pair):
    try:
        low_x = coordinate_pair[0] + 3
        high_x = coordinate_pair[0] + 20
        low_y = coordinate_pair[1] + 3
        high_y = coordinate_pair[1] + 20
        random_x = random.randint(low_x, high_x)
        random_y = random.randint(low_y, high_y)
        randomized_pair = (random_x, random_y)
        print("coordinates to move is " + str(randomized_pair))
        random_duration = random.randrange(10, 35) / 100
        pg.moveTo(randomized_pair, duration=random_duration)
        pg.leftClick()
    except:
        print("Nothing to click")


def take_picture_to_check(screenshot, confidence=0.8, method=cv2.TM_CCOEFF_NORMED):
    piece_img = cv2.imread(screenshot, cv2.IMREAD_UNCHANGED)
    whole_img = ImageGrab.grab()
    whole_hash = np.array(whole_img)
    piece_hash = np.array(piece_img)
    whole_hash = cv2.cvtColor(whole_hash, cv2.COLOR_RGB2BGR)
    piece_hash = cv2.cvtColor(piece_hash, cv2.COLOR_RGB2BGR)

    match = cv2.matchTemplate(whole_hash, piece_hash, method=method)
    # cv2.imshow("s", match)
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()

    min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(match)

    print('Best match top left position: %s' % str(max_loc))
    print('Best match confidence %s' % max_val)
    # pg.moveTo(max_loc, duration=1)

    threshold = confidence
    if max_val >= threshold:
        print("Acceptable")
        return True
    else:
        return False


def move_lower():
    random_duration = random.randrange(5, 15) / 100
    pg.move(0, 30, duration=random_duration)


def press_escape():
    pg2.press('escape')


def press_enter():
    pg.press('enter')


def randomizer_sleep(x):
    y = x + x / 5
    z = x - x / 5
    a = random.uniform(z, y)
    time.sleep(a)


def which_is_likely(first_image, second_image, method=cv2.TM_CCOEFF_NORMED):
    first_img = cv2.imread(first_image, cv2.IMREAD_UNCHANGED)
    second_img = cv2.imread(second_image, cv2.IMREAD_UNCHANGED)
    whole_img = ImageGrab.grab()
    whole_hash = np.array(whole_img)
    first_hash = np.array(first_img)
    second_hash = np.array(second_img)
    whole_hash = cv2.cvtColor(whole_hash, cv2.COLOR_RGB2BGR)

    match_first = cv2.matchTemplate(whole_hash, first_hash, method)
    match_second = cv2.matchTemplate(whole_hash, second_hash, method)

    min_val_first, max_val_first, min_loc_first, max_loc_first = cv2.minMaxLoc(match_first)
    min_val_second, max_val_second, min_loc_second, max_loc_second = cv2.minMaxLoc(match_second)

    if max_val_first > max_val_second:
        print("first image has better collision")
        return first_image
    elif max_val_first == max_val_second:
        print("OH MY GOD THEY ARE EQUAL")
    else:
        print("second image has better collision")
        return second_image


def which_is_likely_triple(first_image, second_image, third_image, method=cv2.TM_CCOEFF_NORMED):
    first_img = cv2.imread(first_image, cv2.IMREAD_UNCHANGED)
    second_img = cv2.imread(second_image, cv2.IMREAD_UNCHANGED)
    third_img = cv2.imread(third_image, cv2.IMREAD_UNCHANGED)
    whole_img = ImageGrab.grab()
    whole_hash = np.array(whole_img)
    first_hash = np.array(first_img)
    second_hash = np.array(second_img)
    third_hash = np.array(third_img)
    whole_hash = cv2.cvtColor(whole_hash, cv2.COLOR_RGB2BGR)

    match_first = cv2.matchTemplate(whole_hash, first_hash, method)
    match_second = cv2.matchTemplate(whole_hash, second_hash, method)
    match_third = cv2.matchTemplate(whole_hash, third_hash, method)

    min_val_first, max_val_first, min_loc_first, max_loc_first = cv2.minMaxLoc(match_first)
    min_val_second, max_val_second, min_loc_second, max_loc_second = cv2.minMaxLoc(match_second)
    min_val_third, max_val_third, min_loc_third, max_loc_third = cv2.minMaxLoc(match_third)

    if max_val_first > max_val_second and max_val_first > max_val_third:
        print("first image has better collision")
        return first_image
    elif max_val_second > max_val_first and max_val_second > max_val_third:
        print("second image has better collision")
        return second_image
    elif max_val_third > max_val_first and max_val_third > max_val_second:
        print("third image has better collision")
        return third_image
    else:
        print("Can't decide which image has better collision")


def check_if_appeared(image_to_found, method=cv2.TM_CCOEFF_NORMED, confidence=0.98):
    status = False

    piece_img = cv2.imread(image_to_found, cv2.IMREAD_UNCHANGED)
    whole_img = ImageGrab.grab()
    whole_hash = np.array(whole_img)
    piece_hash = np.array(piece_img)
    whole_hash = cv2.cvtColor(whole_hash, cv2.COLOR_RGB2BGR)
    # piece_hash = cv2.cvtColor(piece_hash, cv2.COLOR_RGB2BGR)

    match = cv2.matchTemplate(whole_hash, piece_hash, method)

    min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(match)
    print('Operation with ' + str(image_to_found))
    print('Best match top left position: %s' % str(max_loc))
    print('Best match confidence %s' % max_val)
    threshold = confidence

    if max_val >= threshold:
        status = True
        return status
    else:
        print(str(max_val) + " - is maximum confidence value. Threshold is - " + str(threshold))
        return status


def battle_end():
    a = 0
    while a < 24:
        if check_if_appeared(r'elements\button_continue.png', confidence=0.71):
            return True
        elif check_if_appeared(r'elements\s_rank.png', confidence=0.71):
            return True
        else:
            a += 1
            time.sleep(10)
    if a > 23:
        return False


def main_menu():
    a = 0
    while a < 24:
        if check_if_appeared(r'elements\button_battle.png', confidence=0.35):
            return True
        else:
            a += 1
            press_escape()
            time.sleep(2)
    if a > 23:
        return False


def drag_down():
    pg.drag(0, 200, duration=0.5)
    randomizer_sleep(0.5)
    pg.move(0, -200, duration=0.5)


map_up = drag_down


def drag_up():
    pg.drag(0, -200, duration=0.5)
    randomizer_sleep(0.5)
    pg.move(0, 200, duration=0.5)


map_down = drag_up


def drag_right():
    pg.drag(200, 0, duration=0.5)
    randomizer_sleep(0.5)
    pg.move(-200, 0, duration=0.5)


map_left = drag_right


def drag_left():
    pg.drag(-200, 0, duration=0.5)
    randomizer_sleep(2)
    pg.move(200, 0, duration=0.5)


map_right = drag_left


def search_for_image_on_battlemap(image, confidence):
    pg.moveTo(960, 540, duration=0.5)
    if check_if_appeared(image, confidence=confidence):
        return True
    randomizer_sleep(1)
    map_left()
    if check_if_appeared(image, confidence=confidence):
        return True
    randomizer_sleep(1)
    map_up()
    randomizer_sleep(1)
    if check_if_appeared(image, confidence=confidence):
        return True
    randomizer_sleep(1)
    map_up()
    if check_if_appeared(image, confidence=confidence):
        return True
    randomizer_sleep(1)
    map_right()
    randomizer_sleep(1)
    if check_if_appeared(image, confidence=confidence):
        return True
    randomizer_sleep(1)
    map_right()
    if check_if_appeared(image, confidence=confidence):
        return True
    randomizer_sleep(1)
    map_down()
    randomizer_sleep(1)
    if check_if_appeared(image, confidence=confidence):
        return True
    randomizer_sleep(1)
    map_down()
    if check_if_appeared(image, confidence=confidence):
        return True
    randomizer_sleep(1)
    map_left()
    randomizer_sleep(1)


# TODO: click out of image | Done
# TODO: check what color the pixel of coordinates
# TODO:
