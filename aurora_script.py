from actions import recognize_button
import pyautogui as pg
import pydirectinput as pg2
import cv2
import numpy as np
from PIL import ImageGrab
from actions import click_button, randomizer_sleep, which_is_likely, which_is_likely_triple, check_if_appeared, battle_end
from element_click import menu_battle_press, press_escape, press_lclick
import time
import random
from scripts import daily_usual_training, dorm_refilling, daily_fierce_assault, daily_escort_mission
from aurora import press_d3, go_to_map_after_battle, gimme_the_loot, find_enemy


status = "unknown"
a = 0

# recognize_button(r'aurora/siren_big.png', confidence=0.65)
while a < 3:
    pg.moveTo(1920 / 2, 1080 / 2, duration=0.7)
    if check_if_appeared(r'aurora/command_buttons.png', confidence=0.95):
        status = "on_map"
        randomizer_sleep(2)

    if status == "on_map":
        if find_enemy():
            status = 'pre_battle'
            randomizer_sleep(4)

    if status == 'pre_battle':
        click_button(recognize_button(r'aurora/button_battle_mini.png'))
        randomizer_sleep(3)
        status = 'in_battle'

    if status == 'in_battle':
        a = 0
        while a < 24:
            if check_if_appeared(r'aurora\s_rank.png', confidence=0.71):
                a += 24
            elif check_if_appeared(r'aurora\button_continue.png', confidence=0.71):
                a += 24
            randomizer_sleep(11)
            a += 1
        click_button(recognize_button(which_is_likely(r'aurora\s_rank.png', r'aurora\button_continue.png')))
        randomizer_sleep(1)
        gimme_the_loot()
        randomizer_sleep(1)
        go_to_map_after_battle()
        status = 'on_map'
    a += 1