import cv2
import numpy as np
from PIL import ImageGrab
import pyautogui as pg
import random
import pydirectinput as pg2
import inspect
import sys
import time
import threading as th
import keyboard


piece_image = cv2.imread(r'aurora/s_rank.png')
piece_image = np.array(piece_image)

loop_time = time.time()
confidence = 0.8

while True:
    screenshot = ImageGrab.grab()
    screenshot = np.array(screenshot)
    screenshot = cv2.cvtColor(screenshot, cv2.COLOR_RGB2BGR)

    piece_image = np.array(piece_image)

    match = cv2.matchTemplate(screenshot, piece_image, cv2.TM_CCOEFF_NORMED)

    min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(match)
    print('Best match top left position: %s' % str(max_loc))
    print('Best match confidence: %s' % max_val)

    piece_w = piece_image.shape[1]
    piece_h = piece_image.shape[0]

    top_left = max_loc
    bottom_right = (top_left[0] + piece_w, top_left[1] + piece_h)

    if max_val >= confidence:
        cv2.rectangle(screenshot, top_left, bottom_right, color=(0, 255, 0), thickness=2, lineType=cv2.LINE_4)

    cv2.imshow('Computer Vision', screenshot)

    print('FPS {}'.format(1 / (time.time() - loop_time)))
    loop_time = time.time()

    if cv2.waitKey(1) == ord('q'):
        cv2.destroyAllWindows()
        break

print("Done")
