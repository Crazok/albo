from actions import recognize_button
import pyautogui as pg
import cv2
import numpy as np
from PIL import ImageGrab
from actions import click_button, randomizer_sleep, which_is_likely, which_is_likely_triple, check_if_appeared, battle_end
from element_click import menu_battle_press, press_escape, press_lclick, dorm_press, hq_button_press, snacks_5000_press
import time
import random
from scripts import daily_usual_training, dorm_refilling, daily_fierce_assault, daily_escort_mission

""" Dorm refuel code"""

hq_button_press()
randomizer_sleep(1.5)
dorm_press()
randomizer_sleep(5)
# TODO: Confirm_blue click | Done
if check_if_appeared(r'elements\button_dorm_confirm.png'):
    click_button(recognize_button(r'elements\button_dorm_confirm.png', confidence=0.3))
randomizer_sleep(3)
if check_if_appeared(r'elements\give_food.png', confidence=0.5):
    click_button(recognize_button(r'elements\give_food.png', confidence=0.5))
randomizer_sleep(3)
# TODO: while not full... 5k info bar
snacks_5000_press()
# TODO: action, press out of box
# randomizer_sleep(0.5)
# # TODO: realize refuel process by checking attention image
# for i in range(8):
#     press_lclick()
#     randomizer_sleep(0.5)
# pg.move(250, 250, duration=0.5)
# press_lclick()

# main_menu()