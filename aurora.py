from actions import recognize_button
import pyautogui as pg
import pydirectinput as pg2
import cv2
import numpy as np
from PIL import ImageGrab
from actions import click_button, randomizer_sleep, which_is_likely, which_is_likely_triple, check_if_appeared, battle_end, search_for_image_on_battlemap
from element_click import menu_battle_press, press_escape, press_lclick
import time
import random
from scripts import daily_usual_training, dorm_refilling, daily_fierce_assault, daily_escort_mission


def press_d3():
    if check_if_appeared(r'aurora/d3.png', confidence=0.51):
        click_button(recognize_button(r'aurora/d3.png', confidence=0.51))
    randomizer_sleep(1)
    if check_if_appeared(r'aurora/event_go.png', confidence=0.51):
        click_button(recognize_button(r'aurora/event_go.png', confidence=0.51))
    randomizer_sleep(2)
    if check_if_appeared(r'aurora/event_go2.png', confidence=0.51):
        click_button(recognize_button(r'aurora/event_go2.png', confidence=0.51))
    randomizer_sleep(4)


def go_to_map_after_battle():
    while not check_if_appeared(r'aurora/command_buttons.png', confidence=0.95):
        if check_if_appeared(r'aurora/d3.png', confidence=0.91):
            return True
        press_escape()
        randomizer_sleep(3)


def gimme_the_loot():
    if check_if_appeared(r'aurora/gimme_the_loot.png'):
        click_button(recognize_button(r'aurora/gimme_the_loot.png', confidence=0.6))
    else:
        press_lclick()
    randomizer_sleep(1)


def find_enemy():
    # if search_for_image_on_battlemap(r'aurora/siren_big2.png', confidence=0.65):
    #     click_button(recognize_button(r'aurora/siren_big2.png', confidence=0.65))
    #     return True
    if search_for_image_on_battlemap(r'aurora/siren2.png', confidence=0.65):
        click_button(recognize_button(r'aurora/siren2.png', confidence=0.65))
        return True
    elif search_for_image_on_battlemap(r'aurora/siren.png', confidence=0.65):
        click_button(recognize_button(r'aurora/siren.png', confidence=0.65))
        return True
    elif search_for_image_on_battlemap(r'aurora/boss.png', confidence=0.65):
        click_button(recognize_button(r'aurora/boss.png', confidence=0.65))
        return True
    elif search_for_image_on_battlemap(r'aurora/normal_enemy.png', confidence=0.65):
        click_button(recognize_button(r'aurora/normal_enemy.png', confidence=0.65))
        return True
    elif search_for_image_on_battlemap(r'aurora/easy_enemy.png', confidence=0.65):
        click_button(recognize_button(r'aurora/easy_enemy.png', confidence=0.65))
        return True


def press_switch():
    if check_if_appeared(r'aurora/switch.png'):
        click_button(recognize_button(r'aurora/switch.png', confidence=0.6))


def do_battle():
    randomizer_sleep(2)
    click_button(recognize_button(r'aurora/button_battle_mini.png'))
    randomizer_sleep(3)

    a = 0
    while a < 24:
        if check_if_appeared(r'aurora\s_rank.png', confidence=0.71):
            a += 24
        elif check_if_appeared(r'aurora\button_continue.png', confidence=0.71):
            a += 24
        randomizer_sleep(11)
        a += 1

    click_button(recognize_button(which_is_likely(r'aurora\s_rank.png', r'aurora\button_continue.png')))
    randomizer_sleep(1)
    gimme_the_loot()
    randomizer_sleep(1)
    go_to_map_after_battle()