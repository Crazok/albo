from archive.scr03 import WindowCapture
import cv2
import time
import numpy as np
from PIL import ImageGrab

piece_image = cv2.imread(r'aurora/s_rank.png')
piece_image = np.array(piece_image)

logo = cv2.imread(r'elements/memu_play.png')
logo = np.array(logo)

loop_time = time.time()
confidence = 0.8

screenshot = ImageGrab.grab()
screenshot = np.array(screenshot)
screenshot = cv2.cvtColor(screenshot, cv2.COLOR_RGB2BGR)

try:
    match_logo = cv2.matchTemplate(screenshot, logo, cv2.TM_CCOEFF_NORMED)
    min_val_logo, max_val_logo, min_loc_logo, max_loc_logo = cv2.minMaxLoc(match_logo)
    top_left = max_loc_logo[1]
    screenshot = screenshot[max_loc_logo[1] + 30:max_loc_logo[1] + 750, max_loc_logo[0]:max_loc_logo[0] + 1280]
    logo_w_change = max_loc_logo[1]
    logo_h_change = max_loc_logo[0]
except:
    print("Can't find logo")


piece_image = np.array(piece_image)

while True:
    screenshot = ImageGrab.grab()
    screenshot = np.array(screenshot)
    screenshot = cv2.cvtColor(screenshot, cv2.COLOR_RGB2BGR)

    match = cv2.matchTemplate(screenshot, piece_image, cv2.TM_CCOEFF_NORMED)

    min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(match)

    print('Best match top left position: %s' % str(max_loc))
    print('Best match confidence: %s' % max_val)

    piece_w = piece_image.shape[1]
    piece_h = piece_image.shape[0]

    top_left = max_loc
    bottom_right = (top_left[0] + piece_w, top_left[1] + piece_h)

    try:
        screenshot = screenshot[max_loc_logo[1] + 30:max_loc_logo[1] + 750, max_loc_logo[0]:max_loc_logo[0] + 1280]
    except:
        pass

    # Draw a rectangle on our screenshot to highlight where we found the needle.
    # The line color can be set as an RGB tuple
    if max_val >= confidence:
        cv2.rectangle(screenshot, (top_left[0] - logo_h_change, top_left[1] - logo_w_change - 30), (bottom_right[0] - logo_h_change, bottom_right[1] - logo_w_change - 30), color=(0, 255, 0), thickness=2, lineType=cv2.LINE_4)
        # cv2.rectangle(screenshot, (top_left[0], top_left[1]), bottom_right, color=(0, 255, 0), thickness=2, lineType=cv2.LINE_4)

    cv2.imshow('Computer Vision', screenshot)

    print('FPS {}'.format(1 / (time.time() - loop_time)))
    loop_time = time.time()

    if cv2.waitKey(1) == ord('q'):
        cv2.destroyAllWindows()
        break

print("Done")
