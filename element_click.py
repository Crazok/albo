from actions import recognize_button, click_button
import pyautogui as pg
import pydirectinput as pg2


def menu_battle_press():
    click_button(recognize_button(r'elements\button_battle.png', confidence=0.3))


def press_escape():
    pg2.press('escape')


def press_lclick():
    pg.leftClick()



def battle_confirm_press():
    click_button(recognize_button(r'elements\button_confirm.png', confidence=0.6))


def daily_battle_press():
    click_button(recognize_button(r'elements\button_daily.png', confidence=0.3))


def usual_daily_mid_battle_press():
    click_button(recognize_button(r'elements\usual_daily_mid.png', confidence=0.4))


def usual_daily_big_battle_press():
    click_button(recognize_button(r'elements\usual_daily_big.png', confidence=0.4))


def usual_daily_short_battle_press():
    click_button(recognize_button(r'elements\usual_daily_short.png', confidence=0.4))


def usual_daily_shelling_press():
    click_button(recognize_button(r'elements\usual_daily_shelling.png', confidence=0.4))


def battle_mini_press():
    click_button(recognize_button(r'elements\button_battle_mini.png', confidence=0.4))


def hq_button_press():
    click_button(recognize_button(r'elements\button_hq.png', confidence=0.4))


def dorm_press():
    click_button(recognize_button(r'elements\button_dorm.png', confidence=0.4))


def supplies_press():
    click_button(recognize_button(r'elements\button_supplies.png', confidence=0.4))


def snacks_5000_press():
    click_button(recognize_button(r'elements\snacks_5000.png', confidence=0.4))


def fast_access_arrow():
    click_button(recognize_button(r'elements/fast_access_arrow.png', confidence=0.4))